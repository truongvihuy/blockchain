// SPDX-License-Identifier: MIT
pragma solidity ^0.4.26;
pragma experimental ABIEncoderV2;

library SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;

        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}

contract Ownable {
    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    address private _owner;

    constructor() internal {
        _transferOwnership(msg.sender);
    }

    function owner() public view returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    function renounceOwnership() public onlyOwner {
        _transferOwnership(address(0));
    }

    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "Ownable: newOwner is zero address");
        _transferOwnership(newOwner);
    }

    function _transferOwnership(address newOwner) internal {
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

interface IFiatContract {
    function getToken2Price(string _symbol)
        external
        view
        returns (string _symbolToken, uint256 _token2Price);
}

interface IERC721 {
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    function isApprovedForAll(address owner, address operator)
        external
        view
        returns (bool);

    function metadata(uint256 tokenId) external view returns (address creator);
}

interface IERC21 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
    event Fee(
        address indexed from,
        address indexed to,
        address indexed issuer,
        uint256 value
    );

    function totalSupply() external view returns (uint256);

    function balanceOf(address who) external view returns (uint256);

    function issuer() external view returns (address);

    function estimateFee(uint256 value) external view returns (uint256);

    function allowance(address owner, address spender)
        external
        view
        returns (uint256);

    function approve(address spender, uint256 value) external returns (bool);

    function transfer(address to, uint256 value) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool);
}

contract ERC21 is IERC21 {
    using SafeMath for uint256;

    mapping(address => uint256) private _balances;
    uint256 private _minFee;
    address private _issuer;
    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 private _totalSupply;

    modifier onlyIssuer() {
        require(msg.sender == _issuer, "ERC21: caller is not the issuer");
        _;
    }

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address owner) public view returns (uint256) {
        return _balances[owner];
    }

    function issuer() public view returns (address) {
        return _issuer;
    }

    function minFee() public view returns (uint256) {
        return _minFee;
    }

    function estimateFee(uint256 value) public view returns (uint256) {
        return value.mul(0).add(_minFee);
    }

    function allowance(address owner, address spender)
        public
        view
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 value) public returns (bool) {
        require(spender != address(0), "ERC21: approve to the zero address");
        require(
            _balances[msg.sender] >= _minFee,
            "ERC21: transfer amount exceeds balance"
        );

        _allowances[msg.sender][spender] = value;
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
        }
        emit Approval(msg.sender, spender, value);

        return true;
    }

    function transfer(address to, uint256 value) public returns (bool) {
        _transfer(msg.sender, to, value);
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) public returns (bool) {
        uint256 total = value.add(_minFee);
        require(
            total <= _allowances[from][msg.sender],
            "ERC21: transfer amount exceeds allowance"
        );

        _allowances[from][msg.sender] = _allowances[from][msg.sender].sub(
            total
        );
        _transfer(from, to, value);
        if (_minFee > 0) {
            _transfer(from, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function _transfer(
        address from,
        address to,
        uint256 value
    ) internal {
        require(from != address(0), "ERC21: transfer from the zero address");
        require(to != address(0), "ERC21: transfer to the zero address");
        require(
            value <= _balances[from],
            "ERC21: transfer amount exceeds balance"
        );

        _balances[from] = _balances[from].sub(value);
        _balances[to] = _balances[to].add(value);

        emit Transfer(from, to, value);
    }

    function _mint(address account, uint256 value) internal {
        require(account != address(0), "ERC21: mint to the zero address");
        _totalSupply = _totalSupply.add(value);
        _balances[account] = _balances[account].add(value);
        emit Transfer(address(0), account, value);
    }

    function _burn(address account, uint256 value) internal {
        require(account != address(0), "ERC21: burn from the zero address");
        require(
            value <= _balances[account],
            "ERC21: burn amount exceeds balance"
        );

        _totalSupply = _totalSupply.sub(value);
        _balances[account] = _balances[account].sub(value);
        emit Transfer(account, address(0), value);
    }

    function _changeIssuer(address newIssuer) internal {
        require(newIssuer != address(0), "ERC21: newIssuer is zero address");
        _issuer = newIssuer;
    }

    function renounceIssuer() public onlyIssuer {
        _issuer = address(0);
    }

    function transferIssuer(address newIssuer) public onlyIssuer {
        _changeIssuer(newIssuer);
    }

    function _changeMinFee(uint256 value) internal {
        _minFee = value;
    }
}

contract MyERC21 is ERC21 {
    string private _name;
    string private _symbol;
    uint8 private _decimals;

    constructor(string name_, string symbol_) internal {
        _name = name_;
        _symbol = symbol_;
        _decimals = 0;
        _changeIssuer(msg.sender);
    }

    function name() public view returns (string) {
        return _name;
    }

    function symbol() public view returns (string) {
        return _symbol;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }
}

interface IMarket {
    function ceoAddress() external view returns (address);

    function fiatContract() external view returns (address);

    function getTokensFiat(address token)
        external
        view
        returns (string symbol, bool existed);

    function price2wei(uint256 _price, address _fiatBuy)
        external
        view
        returns (uint256);

    function tokenId2wei(
        address _game,
        uint256 _tokenId,
        address _fiatBuy
    ) external view returns (uint256);

    function Percen() external view returns (uint256);

    function getGame(address _game)
        external
        view
        returns (
            uint256 _fee,
            uint256 _limitFee,
            uint256 _creatorFee
        );

    function getGameFees(address _game)
        external
        view
        returns (
            string[] _fees,
            address[] _takers,
            uint256[] _percent,
            uint256 sumGamePercent
        );

    function getGameFeePercent(address _game, string _fee)
        external
        view
        returns (uint256);

    function getTokenPrice(address _game, uint256 _orderId)
        external
        view
        returns (
            address _maker,
            uint256[] _tokenIds,
            uint256 _price,
            address[] _fiat,
            address _buyByFiat,
            bool _isBuy
        );

    function resetPrice4sub(address _game, uint256 _tokenId) external;
}

contract MarketSub is ERC21, MyERC21, Ownable {
    using SafeMath for uint256;

    IMarket public Market;

    constructor(
        address market_
    ) public MyERC21("MarketSub", "MS") {
        setMarket(market_);
    }

    modifier isValidFiatBuy(address _fiat) {
        bool existed;
        (, existed) = Market.getTokensFiat(_fiat);
        require(existed, "Fiat: fiat token is not approved");
        _;
    }

    function setMarket(address _market) public onlyOwner {
        Market = IMarket(_market);
    }

    function calBusinessFee(
        address _game,
        string _symbolFiatBuy,
        uint256 weiPrice
    ) public view returns (uint256 _businessProfit, uint256 _creatorProfit) {
        uint256 fee;
        uint256 limitFee;
        uint256 creatorFee;
        (fee, limitFee, creatorFee) = Market.getGame(_game);
        uint256 businessProfit = (weiPrice.mul(fee)).div(Market.Percen());
        IFiatContract fiatCt = IFiatContract(Market.fiatContract());
        uint256 tokenOnPrice;
        (, tokenOnPrice) = fiatCt.getToken2Price(_symbolFiatBuy);
        uint256 limitFee2Token = (tokenOnPrice.mul(limitFee)).div(1 ether);
        if (weiPrice > 0 && businessProfit < limitFee2Token)
            businessProfit = limitFee2Token;
        uint256 creatorProfit = (weiPrice.mul(creatorFee)).div(Market.Percen());
        return (businessProfit, creatorProfit);
    }

    function buy(
        address _game,
        uint256 _orderId,
        address _fiatBuy,
        string _symbolFiatBuy
    ) public payable isValidFiatBuy(_fiatBuy) {
        address _maker;
        uint256[] memory _tokenIds;
        (_maker, _tokenIds, , , , ) = Market.getTokenPrice(_game, _orderId);
        IERC721 erc721 = IERC721(_game);
        require(
            erc721.isApprovedForAll(_maker, address(this)),
            "MarketSub: sub is not approved for all"
        );
        // pay the fees
        tobuy(_game, _orderId, _fiatBuy, _symbolFiatBuy, _maker, _tokenIds[0]);
        // transfer tokenId
        for (uint256 i = 0; i < _tokenIds.length; i++) {
            erc721.transferFrom(_maker, msg.sender, _tokenIds[i]);
        }
        // reset price in market
        Market.resetPrice4sub(_game, _orderId);
    }

    function tobuy(
        address _game,
        uint256 _orderId,
        address _fiatBuy,
        string _symbolFiatBuy,
        address _maker,
        uint256 tokenId
    ) internal {
        uint256 weiPrice = Market.tokenId2wei(_game, _orderId, _fiatBuy);
        uint256 businessProfit;
        uint256 creatorProfit;
        uint256 sumGamePercent;
        (businessProfit, creatorProfit) = calBusinessFee(
            _game,
            _symbolFiatBuy,
            weiPrice
        );
        (, , , sumGamePercent) = Market.getGameFees(_game);
        uint256 sumGameProfit = (weiPrice.mul(sumGamePercent)).div(
            Market.Percen()
        );
        uint256 ownerProfit = (weiPrice.sub(businessProfit))
            .sub(creatorProfit)
            .sub(sumGameProfit);

        tobuySub(
            _game,
            _fiatBuy,
            weiPrice,
            _maker,
            ownerProfit,
            businessProfit,
            creatorProfit,
            sumGameProfit,
            tokenId
        );
        tobuySub2(_game, _fiatBuy, weiPrice);
    }

    function tobuySub(
        address _game,
        address _fiatBuy,
        uint256 weiPrice,
        address _maker,
        uint256 ownerProfit,
        uint256 businessProfit,
        uint256 creatorProfit,
        uint256 sumGameProfit,
        uint256 tokenId
    ) internal {
        IERC721 erc721 = IERC721(_game);
        address ceo = Market.ceoAddress();

        if (_fiatBuy == address(0)) {
            require(
                weiPrice <= msg.value,
                "MarketSub: transfer amount exceeds balance"
            );
            if (ownerProfit > 0) _maker.transfer(ownerProfit);
            if (businessProfit > 0) ceo.transfer(businessProfit);
            if (creatorProfit > 0) {
                address creator;
                (creator) = erc721.metadata(tokenId);
                creator.transfer(creatorProfit);
            }
        } else {
            IERC21 erc21 = IERC21(_fiatBuy);
            uint256 fee = erc21.estimateFee(weiPrice);
            uint256 totalRequire = fee.add(weiPrice);
            if (businessProfit > 0)
                totalRequire = totalRequire.add(
                    erc21.estimateFee(businessProfit)
                );
            if (creatorProfit > 0)
                totalRequire = totalRequire.add(
                    erc21.estimateFee(creatorProfit)
                );
            if (sumGameProfit > 0)
                totalRequire = totalRequire.add(
                    erc21.estimateFee(sumGameProfit)
                );
            require(
                erc21.transferFrom(msg.sender, address(this), totalRequire),
                "MarketSub: transfer amount exceeds balance"
            );
            if (ownerProfit > 0) erc21.transfer(_maker, ownerProfit);
            if (businessProfit > 0) erc21.transfer(ceo, businessProfit);
            if (creatorProfit > 0) {
                address creatorr;
                (creatorr) = erc721.metadata(tokenId);
                erc21.transfer(creatorr, creatorProfit);
            }
        }
    }

    function tobuySub2(
        address _game,
        address _fiatBuy,
        uint256 weiPrice
    ) internal {
        address[] memory takers;
        uint256[] memory percents;
        (, takers, percents, ) = Market.getGameFees(_game);
        for (uint256 i = 0; i < takers.length; i++) {
            uint256 gameProfit = (weiPrice.mul(percents[i])).div(
                Market.Percen()
            );
            if (_fiatBuy == address(0)) {
                takers[i].transfer(gameProfit);
            } else {
                IERC21 erc21 = IERC21(_fiatBuy);
                erc21.transfer(takers[i], gameProfit);
            }
        }
    }
}
