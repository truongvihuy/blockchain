// SPDX-License-Identifier: MIT
pragma solidity ^0.4.26;
pragma experimental ABIEncoderV2;

library SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;

        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}

contract Ownable {
    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    address private _owner;

    constructor() internal {
        _transferOwnership(msg.sender);
    }

    function owner() public view returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    function renounceOwnership() public onlyOwner {
        _transferOwnership(address(0));
    }

    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "Ownable: newOwner is zero address");
        _transferOwnership(newOwner);
    }

    function _transferOwnership(address newOwner) internal {
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

interface IERC21 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
    event Fee(
        address indexed from,
        address indexed to,
        address indexed issuer,
        uint256 value
    );

    function totalSupply() external view returns (uint256);

    function balanceOf(address account) external view returns (uint256);

    function issuer() external view returns (address);

    function estimateFee(uint256 value) external view returns (uint256);

    function allowance(address owner, address spender)
        external
        view
        returns (uint256);

    function approve(address spender, uint256 value) external returns (bool);

    function transfer(address to, uint256 value) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool);
}

contract ERC21 is IERC21 {
    using SafeMath for uint256;

    mapping(address => uint256) private _balances;
    uint256 private _minFee;
    address private _issuer;
    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 private _totalSupply;

    modifier onlyIssuer() {
        require(msg.sender == _issuer, "ERC21: caller is not the issuer");
        _;
    }

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address owner) public view returns (uint256) {
        return _balances[owner];
    }

    function issuer() public view returns (address) {
        return _issuer;
    }

    function minFee() public view returns (uint256) {
        return _minFee;
    }

    function estimateFee(uint256 value) public view returns (uint256) {
        return value.mul(0).add(_minFee);
    }

    function allowance(address owner, address spender)
        public
        view
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 value) public returns (bool) {
        require(spender != address(0), "ERC21: approve to the zero address");
        require(
            _balances[msg.sender] >= _minFee,
            "ERC21: transfer amount exceeds balance"
        );

        _allowances[msg.sender][spender] = value;
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
        }
        emit Approval(msg.sender, spender, value);

        return true;
    }

    function transfer(address to, uint256 value) public returns (bool) {
        _transfer(msg.sender, to, value);
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) public returns (bool) {
        uint256 total = value.add(_minFee);
        require(
            total <= _allowances[from][msg.sender],
            "ERC21: transfer amount exceeds allowance"
        );

        _allowances[from][msg.sender] = _allowances[from][msg.sender].sub(
            total
        );
        _transfer(from, to, value);
        if (_minFee > 0) {
            _transfer(from, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function _transfer(
        address from,
        address to,
        uint256 value
    ) internal {
        require(from != address(0), "ERC21: transfer from the zero address");
        require(to != address(0), "ERC21: transfer to the zero address");
        require(
            value <= _balances[from],
            "ERC21: transfer amount exceeds balance"
        );

        _balances[from] = _balances[from].sub(value);
        _balances[to] = _balances[to].add(value);

        emit Transfer(from, to, value);
    }

    function _mint(address account, uint256 value) internal {
        require(account != address(0), "ERC21: mint to the zero address");
        _totalSupply = _totalSupply.add(value);
        _balances[account] = _balances[account].add(value);
        emit Transfer(address(0), account, value);
    }

    function _burn(address account, uint256 value) internal {
        require(account != address(0), "ERC21: burn from the zero address");
        require(
            value <= _balances[account],
            "ERC21: burn amount exceeds balance"
        );

        _totalSupply = _totalSupply.sub(value);
        _balances[account] = _balances[account].sub(value);
        emit Transfer(account, address(0), value);
    }

    function _changeIssuer(address newIssuer) internal {
        require(newIssuer != address(0), "ERC21: newIssuer is zero address");
        _issuer = newIssuer;
    }

    function renounceIssuer() public onlyIssuer {
        _issuer = address(0);
    }

    function transferIssuer(address newIssuer) public onlyIssuer {
        _changeIssuer(newIssuer);
    }

    function _changeMinFee(uint256 value) internal {
        _minFee = value;
    }
}

contract MyERC21 is ERC21 {
    string private _name;
    string private _symbol;
    uint8 private _decimals;

    constructor(string name_, string symbol_) internal {
        _name = name_;
        _symbol = symbol_;
        _decimals = 0;
        _changeIssuer(msg.sender);
    }

    function name() public view returns (string) {
        return _name;
    }

    function symbol() public view returns (string) {
        return _symbol;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }
}

contract FiatContract is ERC21, MyERC21, Ownable {
    using SafeMath for uint256;

    event SetPrice(string[] _symbols, uint256[] _token2Price, address _from);
    struct Token {
        string symbol;
        uint256 token2Price;
        bool existed;
    }

    uint256 public mulNum = 2;
    uint256 public lastCode = 3;
    uint256 public callTime = 1;
    uint256 public baseTime = 3;
    uint256 public plusNum = 1;

    mapping(string => Token) private tokens;
    string[] private tokenArr;

    constructor() public MyERC21("FiatContract", "FC") {}

    function setInput(
        uint256 _mulNum,
        uint256 _lastCode,
        uint256 _callTime,
        uint256 _baseTime,
        uint256 _plusNum
    ) public onlyOwner {
        mulNum = _mulNum;
        lastCode = _lastCode;
        callTime = _callTime;
        baseTime = _baseTime;
        plusNum = _plusNum;
    }

    function setPrice(
        string[] _symbols,
        uint256[] _token2Price,
        uint256 _code
    ) public onlyOwner {
        require(_code == findNumber(lastCode), "FIAT: Code is not match");
        for (uint256 i = 0; i < _symbols.length; i++) {
            tokens[_symbols[i]].token2Price = _token2Price[i];
            if (!tokens[_symbols[i]].existed) {
                tokenArr.push(_symbols[i]);
                tokens[_symbols[i]].existed = true;
                tokens[_symbols[i]].symbol = _symbols[i];
            }
        }
        emit SetPrice(_symbols, _token2Price, msg.sender);
    }

    function getToken2Price(string _symbol)
        public
        view
        returns (string _symbolToken, uint256 _token2Price)
    {
        return (tokens[_symbol].symbol, tokens[_symbol].token2Price);
    }

    function getTokenArr() public view returns (string[]) {
        return tokenArr;
    }

    function findNumber(uint256 a) internal returns (uint256) {
        uint256 b = a.mul(mulNum) - plusNum;
        if (callTime % 3 == 0) {
            for (uint256 i = 0; i < baseTime; i++) {
                b += (a + plusNum).div(mulNum);
            }
            b = b.div(baseTime) + plusNum;
        }
        if (b > 9293410619286421) {
            mulNum = callTime % 9 == 1 ? 2 : callTime % 9;
            b = 3;
        }
        ++callTime;
        lastCode = b;
        return b;
    }

    function esfindNumber1(uint256 a)
        public
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        uint256 aa = a.mul(mulNum);
        uint256 aaa = a * 5;
        uint256 b = a.mul(mulNum) - plusNum;
        uint256 c = b;
        for (uint256 i = 0; i < baseTime; i++) {
            c += (a + plusNum).div(mulNum);
        }
        uint256 d = c.div(baseTime) + plusNum;
        return (b, c, d, aa, aaa);
    }

    function esfindNumber(uint256 a) public view returns (uint256) {
        uint256 b = a.mul(mulNum) - plusNum;
        if (callTime % 3 == 0) {
            for (uint256 i = 0; i < baseTime; i++) {
                b += (a + plusNum).div(mulNum);
            }
            b = b.div(baseTime) + plusNum;
        }
        if (b > 9293410619286421) {
            b = 3;
        }
        return b;
    }
}
