pragma solidity ^0.4.26;
pragma experimental ABIEncoderV2;

library SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;

        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}

interface IERC21 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
    event Fee(
        address indexed from,
        address indexed to,
        address indexed issuer,
        uint256 value
    );

    function totalSupply() external view returns (uint256);

    function balanceOf(address who) external view returns (uint256);

    function issuer() external view returns (address);

    function estimateFee(uint256 value) external view returns (uint256);

    function allowance(address owner, address spender)
        external
        view
        returns (uint256);

    function transfer(address to, uint256 value) external returns (bool);

    function approve(address spender, uint256 value) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool);

    function decimals() external view returns (uint8);
}

contract ERC21 is IERC21 {
    using SafeMath for uint256;

    mapping(address => uint256) private _balances;
    uint256 private _minFee;
    address private _issuer;
    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 private _totalSupply;

    constructor() internal {
        _changeIssuer(msg.sender);
    }

    modifier onlyIssuer() {
        require(msg.sender == _issuer, "ERC21: caller is not the issuer");
        _;
    }

    function decimals() public view returns (uint8) {
        return 0;
    }

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address owner) public view returns (uint256) {
        return _balances[owner];
    }

    function issuer() public view returns (address) {
        return _issuer;
    }

    function minFee() public view returns (uint256) {
        return _minFee;
    }

    function estimateFee(uint256 value) public view returns (uint256) {
        return value.mul(0).add(_minFee);
    }

    function allowance(address owner, address spender)
        public
        view
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 value) public returns (bool) {
        require(spender != address(0), "ERC21: approve to the zero address");
        require(
            _balances[msg.sender] >= _minFee,
            "ERC21: transfer amount exceeds balance"
        );

        _allowances[msg.sender][spender] = value;
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
        }
        emit Approval(msg.sender, spender, value);

        return true;
    }

    function transfer(address to, uint256 value) public returns (bool) {
        _transfer(msg.sender, to, value);
        if (_minFee > 0) {
            _transfer(msg.sender, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) public returns (bool) {
        uint256 total = value.add(_minFee);
        require(
            total <= _allowances[from][msg.sender],
            "ERC21: transfer amount exceeds allowance"
        );

        _allowances[from][msg.sender] = _allowances[from][msg.sender].sub(
            total
        );
        _transfer(from, to, value);
        if (_minFee > 0) {
            _transfer(from, _issuer, _minFee);
            emit Fee(msg.sender, to, _issuer, _minFee);
        }
        return true;
    }

    function _transfer(
        address from,
        address to,
        uint256 value
    ) internal {
        require(from != address(0), "ERC21: transfer from the zero address");
        require(to != address(0), "ERC21: transfer to the zero address");
        require(
            value <= _balances[from],
            "ERC21: transfer amount exceeds balance"
        );

        _balances[from] = _balances[from].sub(value);
        _balances[to] = _balances[to].add(value);

        emit Transfer(from, to, value);
    }

    function _mint(address account, uint256 value) internal {
        require(account != address(0), "ERC21: mint to the zero address");
        _totalSupply = _totalSupply.add(value);
        _balances[account] = _balances[account].add(value);
        emit Transfer(address(0), account, value);
    }

    function _burn(address account, uint256 value) internal {
        require(account != address(0), "ERC21: burn from the zero address");
        require(
            value <= _balances[account],
            "ERC21: burn amount exceeds balance"
        );

        _totalSupply = _totalSupply.sub(value);
        _balances[account] = _balances[account].sub(value);
        emit Transfer(account, address(0), value);
    }

    function _changeIssuer(address newIssuer) internal {
        require(newIssuer != address(0), "ERC21: newIssuer is zero address");
        _issuer = newIssuer;
    }

    function renounceIssuer() public onlyIssuer {
        _issuer = address(0);
    }

    function transferIssuer(address newIssuer) public onlyIssuer {
        _changeIssuer(newIssuer);
    }

    function _changeMinFee(uint256 value) internal {
        _minFee = value;
    }
}

contract Ownable {
    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    address private _owner;

    constructor() internal {
        _transferOwnership(msg.sender);
    }

    function owner() public view returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    function renounceOwnership() public onlyOwner {
        _transferOwnership(address(0));
    }

    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "Ownable: newOwner is zero address");
        _transferOwnership(newOwner);
    }

    function _transferOwnership(address newOwner) internal {
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

contract IERC721 {
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public;

    function isApprovedForAll(address owner, address operator)
        public
        view
        returns (bool);

    function ownerOf(uint256 tokenId) public view returns (address owner);

    function metadata(uint256 tokenId) public view returns (address creator);

    function transfer(address to, uint256 tokenId) public;

    function getApproved(uint256 tokenId)
        public
        view
        returns (address operator);
}

contract Exchange is ERC21, Ownable {
    event MakeOrder(address maker, uint256 index, string orderId);
    event CancelOrder(address maker, uint256 index);
    event ExchangeNFT(
        address sender,
        uint256 index,
        IERC721 NFTTo,
        uint256 tokenIdTo
    );

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    constructor(string memory name_, string memory symbol_) public {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
        _changeIssuer(msg.sender);
    }

    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }

    function setMinFee(uint256 value) public onlyIssuer {
        _changeMinFee(value);
    }

    function burn(uint256 value) public returns (bool) {
        _burn(msg.sender, value);
        return true;
    }

    // ==================
    using SafeMath for uint256;
    IERC21 public feeToken = IERC21(0x18d4d562465DF77DA8171Ec244eA21b1DBBAE0D6);
    address public signer = 0x64470E5F5DD38e497194BbcAF8Daa7CA578926F6;
    uint256 public feeExchange = 40;
    uint256 public panaltyPercent = 20;
    modifier onlySigner() {
        require(signer == msg.sender);
        _;
    }
    struct order {
        string _orderId;
        IERC721 _NFTFrom;
        uint256 _tokenIdFrom;
        uint256 status; // 1 waiting; 2 finish; 3 canceled
        string _type;
    }
    mapping(address => order[]) public orders;

    function getMessageHash(uint256 timestamp, uint256 _tokenIdTo)
        public
        pure
        returns (bytes32)
    {
        return keccak256(abi.encodePacked(timestamp, _tokenIdTo));
    }

    function getEthSignedMessageHash(bytes32 _messageHash)
        public
        pure
        returns (bytes32)
    {
        return
            keccak256(
                abi.encodePacked(
                    "\x19Ethereum Signed Message:\n32",
                    _messageHash
                )
            );
    }

    function permit(
        uint256 timestamp,
        uint256 _tokenIdTo,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return
            ecrecover(
                getEthSignedMessageHash(getMessageHash(timestamp, _tokenIdTo)),
                v,
                r,
                s
            ) == signer;
    }

    function getOrders(address _from) public view returns (order[] memory) {
        return orders[_from];
    }

    function getFee() public view returns (uint256) {
        uint256 decimal = feeToken.decimals();
        return feeExchange.mul(10**decimal);
    }

    function makeOrder(
        string _orderId,
        IERC721 _NFTFrom,
        uint256 _tokenIdFrom,
        string _type
    ) public {
        require(feeToken.transferFrom(msg.sender, address(this), getFee()));
        orders[msg.sender].push(
            order(_orderId, _NFTFrom, _tokenIdFrom, 1, _type)
        );
        emit MakeOrder(msg.sender, orders[msg.sender].length - 1, _orderId);
    }

    function cancelOrder(uint256 _index) public {
        require(orders[msg.sender][_index].status == 1);
        require(
            feeToken.transfer(
                msg.sender,
                getFee().mul(100 - panaltyPercent).div(100)
            )
        );
        orders[msg.sender][_index].status = 3;
        emit CancelOrder(msg.sender, _index);
    }

    function exchange(
        address _From,
        uint256 _index,
        IERC721 _NFTTo,
        uint256 _tokenIdTo,
        uint256 timestamp,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public {
        require(orders[_From][_index].status == 1);
        require(permit(timestamp, _tokenIdTo, v, r, s));
        require(feeToken.transferFrom(msg.sender, address(this), getFee()));
        _NFTTo.transferFrom(msg.sender, _From, _tokenIdTo);
        orders[_From][_index]._NFTFrom.transferFrom(
            _From,
            msg.sender,
            orders[_From][_index]._tokenIdFrom
        );
        orders[_From][_index].status = 2;
        emit ExchangeNFT(msg.sender, _index, _NFTTo, _tokenIdTo);
    }

    function configSigner(address _signer) public onlySigner {
        signer = _signer;
    }

    function config(
        IERC21 _feeToken,
        uint256 _feeExchange,
        uint256 _panaltyPercent
    ) public onlyOwner {
        feeToken = _feeToken;
        feeExchange = _feeExchange;
        panaltyPercent = _panaltyPercent;
    }

    function withdraw(
        IERC21 _token,
        uint256 _amount,
        address _to
    ) public onlyOwner {
        _token.transfer(_to, _amount);
    }
}
