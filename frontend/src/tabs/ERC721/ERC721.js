import React from 'react'
import { Box, Typography } from '@mui/material'
import { useWeb3 } from 'contexts/web3.provider'
import { MessageErrorTab } from 'components'
import abi from 'abi/ERC721.json'


export default function TabContainer({ contractAddress, chainId }) {
  const { signer } = useWeb3()

  if (!(contractAddress && chainId && abi)) {
    return <MessageErrorTab />
  }
  return (
    <Box>
      <Typography>ERC721 -  {contractAddress}</Typography>
    </Box>
  )
}