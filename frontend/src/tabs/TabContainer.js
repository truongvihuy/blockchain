import React from 'react'
import { Box } from '@mui/material'
import { ConnectWallet, MenuAddTab, NavigationTab } from 'components'
import { TabsTemplate } from 'config/tab.config'
import { useTabs } from 'contexts/tab.provider'
import { ContainerStyled, NavigationStyled } from './TabContainer.styled'

const NewTab = React.lazy(() => import('tabs/utils/NewTab'))


export default function TabContainer(props) {
  const { tabs, activeTab, openTab, addTab, closeTab, templates } = useTabs()
  const Component = TabsTemplate[tabs[activeTab]?.template]

  return (
    <Box>
      <NavigationStyled color='primary'>
        <NavigationTab tabs={tabs} activeTab={activeTab}
          openTab={openTab} closeTab={closeTab} />
        <MenuAddTab onAddTab={addTab} templates={templates} />

        <ConnectWallet />
      </NavigationStyled>

      <ContainerStyled component={'main'}>
        <React.Suspense fallback={'loading'}>
          {Component
            ? <Component key={activeTab} {...tabs[activeTab].props} />
            : <NewTab />}
        </React.Suspense>
      </ContainerStyled>
    </Box>
  )
}