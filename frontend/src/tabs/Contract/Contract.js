import React from 'react'
import { Box, Typography } from '@mui/material'
import { useWeb3 } from 'contexts/web3.provider'
import { MessageErrorTab } from 'components'


export default function TabContainer({ contractAddress, chainId, abi }) {
  const { signer } = useWeb3()

  if (!(contractAddress && chainId && abi)) {
    return <MessageErrorTab />
  }
  return (
    <Box>
      <Typography>Contract -  {contractAddress}</Typography>
    </Box>
  )
}