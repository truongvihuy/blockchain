import styled from '@emotion/styled'
import { Container, AppBar } from '@mui/material'


export const NavigationStyled = styled(AppBar)({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center'
})

export const ContainerStyled = styled(Container)({
  marginTop: 50,
  paddingTop: 24,
})
