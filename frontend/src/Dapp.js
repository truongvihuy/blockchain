import React from 'react'
import { SnackbarProvider } from 'notistack'
import { TabsConfig, TemplatesConfig } from 'config/tab.config'
import { DappConfig } from 'config/dapp.config'
import { TabsProvider } from 'contexts/tab.provider'
import { Web3Provider } from 'contexts/web3.provider'
import TabContainer from 'tabs'


export default function Dapp() {
  return (
    <SnackbarProvider maxSnack={DappConfig.maxToast}>
      <Web3Provider
        defaultChainId={DappConfig.chain.defaultChainId}
        chainIdsAllowed={DappConfig.chain.chainIdsAllowed}
        connectMethodsAllowed={DappConfig.chain.connectMethodsAllowed}
      >
        <TabsProvider
          type={DappConfig.navigation.type}
          tabs={TabsConfig}
          templates={TemplatesConfig}
        >
          <TabContainer />
        </TabsProvider>
      </Web3Provider>
    </SnackbarProvider>
  )
}