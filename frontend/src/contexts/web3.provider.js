import React from 'react'
import services, { MetamaskService, PantographService, WalletConnectService } from 'web3/services'
import { CHAIN_INFO, CONNECTION_METHOD, RPCs } from 'web3/constants'


const Web3Context = React.createContext({})
export const Web3Provider = ({
  defaultChainId = null,
  connectMethodsAllowed = null, chainIdsAllowed = null,
  children
}) => {
  const [signer, setSigner] = React.useState(null)
  const [chain, setChain] = React.useState(null)
  const [connectMethod, setConnectMethod] = React.useState(null)

  const web3 = connectMethod && services[connectMethod].web3()

  const checkConnectMethodAllowed = (_connectMethod, returnBoolean = false) => {
    if (!services[_connectMethod]) {
      throw new Error(`Connecet Method '${_connectMethod}' not support`)
    }
    if (connectMethodsAllowed && connectMethodsAllowed.length) {
      if (!connectMethodsAllowed.includes(_connectMethod)) {
        if (returnBoolean) {
          return false
        }
        throw new Error(`Connecet Method '${_connectMethod}' not allowed`)
      }
    }
    if (returnBoolean) {
      return true
    }
  }

  const checkChainIdsAllowed = (_chainId, returnBoolean = false) => {
    if (!RPCs[_chainId]) {
      throw new Error(`ChainId '${_chainId}' not support`)
    }
    if (chainIdsAllowed && chainIdsAllowed.length) {
      if (!chainIdsAllowed.includes(_chainId.toString())) {
        if (returnBoolean) {
          return false
        }
        throw new Error(`ChainId '${_chainId}' not allowed`)
      }
    }
  }

  const signIn = async (_connectMethod) => {
    checkConnectMethodAllowed(_connectMethod) // throw error when not support connect method

    const accounts = await services[_connectMethod].signIn()
    if (!accounts || !accounts[0]) {
      throw new Error('Sign in Fail !!!')
    }

    const networkAndChainId = await services[_connectMethod].getNetworkAndChainId()
    try {
      checkChainIdsAllowed(networkAndChainId.chainId) // throw error when not support networkId
    } catch (err) {
      // change network
      if (defaultChainId && networkAndChainId.chainId !== defaultChainId) {
        await _changeChain(defaultChainId, _connectMethod)
        return signIn(_connectMethod)
      } else {
        throw err
      }
    }
    const rpc = RPCs[networkAndChainId.chainId]
    const infoChain = CHAIN_INFO[networkAndChainId.chainId]


    if (connectMethod !== _connectMethod) {
      setConnectMethod(_connectMethod)
    }
    if (!chain || rpc.chainId !== chain.chainId) {
      setChain({ ...rpc, ...infoChain })
    }
    setSigner({
      address: accounts[0],
      accounts,
    })
  }

  const signOut = () => {

  }

  const changeChain = async (chainId) => {
    await _changeChain(chainId, connectMethod)
  }

  const _changeChain = async (chainId, _connectMethod) => {
    await services[_connectMethod].changeChainId(chainId)
  }

  const reset = () => {
    setSigner(null)
    setConnectMethod(null)
    setChain(null)
  }

  const test = async () => {
    console.log(PantographService.web3())
    await signIn(CONNECTION_METHOD.PANTOGRAPH)
  }

  return (
    <Web3Context.Provider value={{
      web3, signer, chain, connectMethod,
      signIn, signOut, changeChain, reset, test,
    }}>
      {children}
    </Web3Context.Provider>
  )
}
export const useWeb3 = () => React.useContext(Web3Context)