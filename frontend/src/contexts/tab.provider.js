import React from 'react'


const tabsKey = 'tabs'

const TabsContext = React.createContext({})
export const TabsProvider = ({ tabs = null, templates = null, type, children }) => {
  const [tabList, setTabList] = React.useState(tabs ?? [])
  const [activeTab, setActiveTab] = React.useState(0)

  React.useEffect(() => {
    try {
      let _tabs = JSON.parse(localStorage.getItem(tabsKey))
      if (_tabs && Array.isArray(_tabs)) {
        setTabList(_tabs)
      }
    } catch (e) { }
  }, [])

  const setLocalStorage = (_tabs) => {
    const tabsString = JSON.stringify(_tabs)
    localStorage.setItem(tabsKey, tabsString)
  }

  const addTab = (tab) => {
    const newTabs = [...tabList]
    newTabs.push(tab)
    setTabList(newTabs)
    setActiveTab(newTabs.length - 1)
    setLocalStorage(newTabs)
  }

  const closeTab = (tabIndex) => {
    let newTabs = [...tabList]
    newTabs.splice(tabIndex, 1)
    setTabList(newTabs)
    if (activeTab >= newTabs.length) {
      setActiveTab(newTabs.length - 1)
    }
    setLocalStorage(newTabs)
  }

  const openTab = (tabIndex) => {
    if (tabIndex !== activeTab && 0 <= tabIndex && tabIndex < tabList.length) {
      setActiveTab(tabIndex)
    }
  }


  return (
    <TabsContext.Provider value={{
      tabs: tabList, activeTab, type, templates,
      addTab, closeTab, openTab
    }}>
      {children}
    </TabsContext.Provider>
  )
}
export const useTabs = () => React.useContext(TabsContext)
