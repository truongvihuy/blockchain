import React, { useImperativeHandle } from 'react'
import { Box, Button, Modal, Typography } from '@mui/material'
import InputContract from 'components/InputContract'
import { useParamsContracts } from 'hooks/useParamsContracts'


const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '400px',
  bgcolor: 'background.paper',
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
}

const ModalAddTab = (props, ref) => {
  const [isOpen, setIsOpen] = React.useState(false)
  const {
    params, onChangeParams,
    onChangeInputs, wrapperSubmit
  } = useParamsContracts()

  React.useEffect(() => {
    onChangeInputs(props.template?.inputs ?? [])
  }, [isOpen && props.template])

  useImperativeHandle(ref, () => ({
    toggle() {
      setIsOpen(!isOpen)
    }
  }))

  const handleClick = () => {
    wrapperSubmit((propParams) => {
      setIsOpen(false)
      props.onAdd({
        label: `${props.template.label ?? props.template.template} - ${propParams.contractAddress}`,
        template: props.template.template,
        props: propParams,
      })
    })
  }

  return (
    <>
      {!props.hiddenButton && <Button onClick={() => setIsOpen(true)}>ahds</Button>}

      <Modal
        hideBackdrop
        open={isOpen}
        onClose={() => setIsOpen(false)}
        aria-labelledby='child-modal-title'
        aria-describedby='child-modal-description'
      >
        {props.template
          ? (
            <Box sx={style}>
              <Typography>{props.template.label ?? props.template.template}</Typography>
              <div>
                {props.template.inputs.map((e, i) =>
                  <InputContract key={i} {...e}
                    value={params[e.name]} onChange={onChangeParams} />
                )}
              </div>

              <div>
                <Button variant='outlined' sx={{ margin: '10px' }}
                  onClick={() => setIsOpen(false)}>Close</Button>
                <Button variant='contained'
                  onClick={handleClick}>Add</Button>
              </div>
            </Box>)
          : <></>}
      </Modal>
    </>
  )
}

export default React.forwardRef(ModalAddTab)