import React from 'react'
import { Divider, IconButton, Menu, MenuItem } from '@mui/material'
import { Add } from '@mui/icons-material'
import { useTabs } from 'contexts/tab.provider'
import ModalAddTab from './ModalAddTab'


export default function MenuAddTab(props) {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [templateChosen, setTemplateChosen] = React.useState(null)
  const refModal = React.useRef()

  const isOpenMenu = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleSelect = (t) => {
    setAnchorEl(null)
    setTemplateChosen(t)
    if (refModal.current) {
      refModal.current.toggle()
    }
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div>
      <IconButton
        aria-controls={isOpenMenu ? 'basic-menu' : undefined}
        aria-haspopup='true'
        aria-expanded={isOpenMenu ? 'true' : undefined}
        onClick={handleClick}
      >
        <Add sx={{ color: 'white' }} />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={isOpenMenu}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {props.templates.map((e, i) => e.divider
          ? <Divider key={i} />
          : (
            <MenuItem key={i} onClick={() => handleSelect(e)}>
              {e.label}</MenuItem>
          ))}
      </Menu>
      <ModalAddTab ref={refModal} hiddenButton template={templateChosen}
        onAdd={props.onAddTab} />
    </div>
  )
}