import React from 'react'
import { TextField } from '@mui/material'


const mapTypeInput = {
  uint256: 'number',
  number: 'number',
  address: 'text',
  ['uint256[]']: 'text',
  ['address[]']: 'text',
}

export default function InputContract({ type, name, label, value, options, onChange }) {

  const handleChange = (e) => {
    onChange(e.target.name, e.target.value)
  }

  return !options || !options.length ? (
    <TextField variant='standard' sx={{ marginY: '8px' }} fullWidth
      label={label ?? name} name={name}
      type={mapTypeInput[type]}
      value={value ?? ''} onChange={handleChange}
      onFocus={e => e.target.select()}
    />
  ) : (
    <TextField variant='standard' sx={{ marginY: '8px' }} fullWidth
      label={label ?? name} name={name}
      select SelectProps={{
        native: true,
      }}
      value={value ?? ''} onChange={handleChange}
    >
      {options.map(e => (
        <option key={e.value} value={e.value}>
          {e.label}
        </option>
      ))}
    </TextField>
  )
}