import React from 'react'
import { Login } from '@mui/icons-material'
import { useWeb3 } from 'contexts/web3.provider'
import { CONNECTION_METHOD } from 'web3/constants'
import { ellipsisAddress } from 'utils'
import IconChain from 'components/IconChain'
import InfoLogin from './InfoLogin'
import { ButtonConnectWalletStyled, ConnectWalletWrapperStyled } from './ConnectWallet.styled'


export default function ConnectWallet() {
  const { signer, signIn, signOut, chain } = useWeb3()

  return (
    <ConnectWalletWrapperStyled>
      {signer
        ? (
          <>
            <ButtonConnectWalletStyled onClick={null}>
              <IconChain icon={chain.chainIcon} /> {ellipsisAddress(signer.address)}
            </ButtonConnectWalletStyled>
          </>
        ) : (
          <ButtonConnectWalletStyled
            variant='text' sx={{ color: 'white' }}
            onClick={() => signIn(CONNECTION_METHOD.METAMASK)}
          >
            <Login /> <span>Sign In</span>
          </ButtonConnectWalletStyled>
        )}
    </ConnectWalletWrapperStyled>
  )
}