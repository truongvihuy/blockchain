import styled from '@emotion/styled'
import { Button } from '@mui/material'


export const ConnectWalletWrapperStyled = styled.div({
  padding: 4,
})

export const ButtonConnectWalletStyled = styled(Button)({
  border: '1px solid white',
  color: 'white',
})