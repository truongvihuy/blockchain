import styled from '@emotion/styled'
import { Tabs, Tab } from '@mui/material'


export const NavTabStyled = styled(Tabs)({
  width: '80%',
})

export const NavTabItemStyled = styled(Tab)((props) => (
  {
    backgroundColor: props.active ? '#014f9c' : undefined
  }
))