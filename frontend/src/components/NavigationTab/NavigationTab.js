import NavigationTabItem from './NavigationTabItem'
import { NavTabStyled } from './NavigationTab.styled'


export default function NavigationTab({ tabs, activeTab, type, openTab, closeTab }) {

  const handleTab = (e, newValue) => {
    openTab(newValue)
  }

  return (
    <NavTabStyled variant='scrollable' scrollButtons='auto' allowScrollButtonsMobile
      value={+activeTab} onChange={handleTab}>
      {tabs.map((tab, tabIndex) => (
        <NavigationTabItem key={tabIndex} active={activeTab === tabIndex}
          type={type} label={tab.label} template={tab.template}
          propsContainer={tab.props} closeTab={() => closeTab(tabIndex)} onClick={(e) => handleTab(e, tabIndex)}
        />
      ))}
    </NavTabStyled>
  )
}