import { Close } from '@mui/icons-material'
import { IconButton } from '@mui/material'
import IconChain from 'components/IconChain/IconChain'
import { NavTabItemStyled } from './NavigationTab.styled'


export default function NavigationTabItem({ propsContainer, template, label, value, type, closeTab, className, active, onClick }) {
  let labelComponent = (
    <div>
      <span>
        {propsContainer.chainId && <IconChain icon={propsContainer.icon} />}
        {label ? label : `${template} - ${propsContainer.contractAddress}`}
      </span>
      {type !== 'tabs' && <IconButton size='small' sx={{ padding: 0, marginX: '4px' }}>
        <Close onClick={closeTab} sx={{ color: 'white' }} />
      </IconButton>}
    </div>
  )
  return <NavTabItemStyled className={className} active={active} label={labelComponent} sx={{ opacity: 1 }} value={value} onClick={onClick} />
}