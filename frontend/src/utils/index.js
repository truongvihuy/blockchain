export const convertNumberToHex = (number, padding = 2) => {
  var hex = Number(number).toString(16)
  while (hex.length < padding) {
    hex = `0${hex}`
  }

  return `0x${hex}`
}

export const ellipsisAddress = (
  address,
  prefixLength = 6,
  suffixLength = 4,
) => {
  return `${address.substr(0, prefixLength)}...${address.substr(
    address.length - suffixLength,
    suffixLength,
  )}`
}