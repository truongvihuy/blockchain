export default class BaseService {
  static web3() {
    throw new Error('Abstract Method \'web3\' has no implementation')
  }

  static signIn() {
    throw new Error('Abstract Method \'signIn\' has no implementation')
  }

  static getNetworkAndChainId() {
    throw new Error('Abstract Method \'getNetworkAndChainId\' has no implementation')
  }

  static addChainId() {
    throw new Error('Abstract Method \'addChainId\' has no implementation')
  }

  static changeChainId(chainId) {
    throw new Error('Abstract Method \'changeChainId\' has no implementation')
  }
}