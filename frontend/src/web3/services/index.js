import { CONNECTION_METHOD } from 'web3/constants'

import MetamaskService from './metamask'
import PantographService from './pantograph'
import WalletConnectService from './wallet-connect'

export default {
  [CONNECTION_METHOD.METAMASK]: MetamaskService,
  // [CONNECTION_METHOD.PANTOGRAPH]: PantographService,
  // [CONNECTION_METHOD.WALLET_CONNECT]: WalletConnectService,
}

export {
  MetamaskService, PantographService, WalletConnectService
}