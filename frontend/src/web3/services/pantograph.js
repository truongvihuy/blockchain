import BaseService from './extension.base'

export default class PantographService extends BaseService {
  static web3() {
    return window.tomoWeb3 || window.web3
  }

  static checkInstalled() {
    if (!this.web3()) {
      throw new Error('Install Pantograph')
    }
  }

  static signIn() {
    this.checkInstalled()

    return this.web3().personal.sign('0x65C2f3acceC21fA5bd5869572273dA7b7296AdEA',)
      // .request({ method: 'eth_requestAccounts' })
  }
}