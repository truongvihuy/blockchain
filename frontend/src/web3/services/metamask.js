import MetaMaskOnboarding from '@metamask/onboarding'
import { convertNumberToHex } from 'utils'
import { RPCs } from 'web3/constants'
import BaseService from './extension.base'


export default class MetamaskService extends BaseService {
  static onboarding
  static web3() {
    return window.ethereum
  }

  static checkInstalled() {
    if (!this.onboarding) {
      this.onboarding = new MetaMaskOnboarding()
    }
    if (!MetaMaskOnboarding.isMetaMaskInstalled()) {
      this.onboarding.startOnboarding()
      throw new Error('Install MetaMask')
    }
  }

  static signIn() {
    this.checkInstalled()

    return this.web3()
      .request({ method: 'eth_requestAccounts' })
  }

  static async getNetworkAndChainId() {
    this.checkInstalled()

    const [chainId, networkId] = await Promise.all([
      this.web3().request({
        method: 'eth_chainId'
      }),
      this.web3().request({
        method: 'net_version'
      })
    ])
    return { chainId: +chainId, networkId: +networkId }
  }

  static async addChainId(chainData) {
    this.checkInstalled()

    return this.web3().request({
      method: 'wallet_addEthereumChain', params: [chainData]
    })
  }

  static async changeChainId(chainId) {
    this.checkInstalled()

    await this.web3().request({
      method: 'wallet_switchEthereumChain',
      params: [{ chainId: convertNumberToHex(chainId) }]
    }).catch(async (err) => {
      // add chain when metamask not exist this chain
      if (err.code === 4902) {
        const chainData = RPCs[chainId]
        if (chainData) {
          await this.addChainId(chainData)
        } else {
          throw new Error(`ChainId '${chainId}' not support`)
        }
      } else {
        throw err
      }
    })
  }
}