import { CHAIN_ID, RPCs } from 'web3/constants'


export function isTomoChain(chainId) {
  // const chainId = network.config.chainId
  return [CHAIN_ID.TomoChain_Mainnet, CHAIN_ID.TomoChain_Testnet].includes(chainId)
}

export function isPolygon(chainId) {
  // const chainId = network.config.chainId
  return [CHAIN_ID.Polygon_Mainnet, CHAIN_ID.Polygon_Testnet].includes(chainId)
}

export function isBSC(chainId) {
  // const chainId = network.config.chainId
  return [CHAIN_ID.Bsc_Mainnet, CHAIN_ID.Bsc_Testnet].includes(chainId)
}

export function isEthereum(chainId) {
  // const chainId = network.config.chainId
  return [CHAIN_ID.Ethereum_Mainnet, CHAIN_ID.Rinkeby_Testnet].includes(chainId)
}

export function isBacoor(chainId) {
  return [CHAIN_ID.Bacoor_Testnet].includes(chainId)
}

export function isLocalNetwork(chainId) {
  // const chainId = network.config.chainId
  return !Object.values(CHAIN_ID).includes(chainId)
}

export function transactionPage(chainId, tx) {
  // const chainId = network.config.chainId
  return `${RPCs[chainId]?.blockExplorerUrls?.[0] ?? '.'}/tx/${tx.transactionHash}`
}

export function tokenPage(chainId, address) {
  // const chainId = network.config.chainId
  return `${RPCs[chainId]?.blockExplorerUrls?.[0] ?? '.'}/token/${address}`
}

export function addressPage(chainId, address) {
  // const chainId = network.config.chainId
  return `${RPCs[chainId]?.blockExplorerUrls?.[0] ?? '.'}/address/${address}`
}