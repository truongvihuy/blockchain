import { useState } from 'react'

export const useParamsContracts = (defaultInputs = [], defaultParams = {}) => {
  const [params, setParams] = useState(defaultParams)
  const [inputs, setInputs] = useState(defaultInputs)

  const cleanParams = () => setParams({})
  const onChangeParams = (key, value) => {
    let newParamsState = { ...params }
    newParamsState[key] = value
    setParams(newParamsState)
  }

  const onChangeInputs = (inputs) => {
    setInputs(inputs)
    cleanParams()
  }

  const wrapperSubmit = (callback) => {
    return callback(params)
  }

  return {
    params, cleanParams, onChangeParams,
    inputs, onChangeInputs, wrapperSubmit
  }
}