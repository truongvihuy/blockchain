import React from 'react'
import { RPCs } from 'web3/constants'
import { DappConfig } from 'config/dapp.config'


const ERC20 = React.lazy(() => import('tabs/ERC20'))
const ERC21 = React.lazy(() => import('tabs/ERC21'))
const ERC721 = React.lazy(() => import('tabs/ERC721'))
const Fiat = React.lazy(() => import('tabs/Fiat'))
const Market = React.lazy(() => import('tabs/Market'))
const MarketSub = React.lazy(() => import('tabs/MarketSub'))
const ContractComponent = React.lazy(() => import('tabs/Contract'))


export const TabsTemplate = {
  'ERC20': ERC20,
  'BEP20': ERC20,
  'TRC20': ERC20,
  'TRC21': ERC21,
  'ERC721': ERC721,
  'TRC721': ERC721,
  'BEP721': ERC721,
  'FIAT': Fiat,
  'MARKET': Market,
  'MARKET_SUB': MarketSub,
  'Contract': ContractComponent,
}

const OPTION_CHAIN_ID = DappConfig.chainIdsAllowed?.length > 0
  ? DappConfig.chainIdsAllowed.map(chainId => ({
    label: RPCs[+chainId].chainName,
    value: +chainId,
  }))
  : Object.values(RPCs).map(rpc => ({
    label: rpc.chainName,
    value: +rpc.chainId,
  }))

const INPUTS_PRIMARY = [
  { name: 'contractAddress', label: 'Contract Address', type: 'address' },
  { name: 'chainId', label: 'Chain Id', type: 'uint256', options: OPTION_CHAIN_ID },
]

const INPUTS_ABI = [
  ...INPUTS_PRIMARY,
  { name: 'abi', label: 'ABI', type: 'textarea' }
]

export const TemplatesConfig = [
  {
    label: 'ERC20',
    template: 'ERC20',
    inputs: INPUTS_PRIMARY,
  },
  {
    label: 'ERC21',
    template: 'ERC21',
    inputs: INPUTS_PRIMARY,
  },
  {
    label: 'ERC721',
    template: 'ERC721',
    inputs: INPUTS_PRIMARY,
  },
  {
    divider: true,
  },
  {
    label: 'Fiat',
    template: 'FIAT',
    inputs: INPUTS_PRIMARY,
  },
  {
    label: 'Market',
    template: 'MARKET',
    inputs: INPUTS_PRIMARY,
  },
  {
    label: 'MarketSub',
    template: 'MARKET_SUB',
    inputs: INPUTS_PRIMARY,
  },
  {
    divider: true,
  },
  {
    label: 'Contract',
    template: 'Contract',
    inputs: INPUTS_ABI,
  },
]

export const TabsConfig = [
]