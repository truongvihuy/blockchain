export const DappConfig = {
  chain: {
    defaultChainId: +process.env.REACT_APP_DEFAULT_CHAIN_ID,
    connectMethodsAllowed: process.env.REACT_APP_CONNECT_METHODS_ALLOWED ? process.env.REACT_APP_CONNECT_METHODS_ALLOWED.split(',') : [],
    chainIdsAllowed: process.env.REACT_APP_CHAIN_IDS_ALLOWED ? process.env.REACT_APP_CHAIN_IDS_ALLOWED.split(',') : [],
  },
  navigation: {
    type: 'template',
  },
  maxToast: 5,
}