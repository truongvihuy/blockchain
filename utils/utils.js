function convertNumberToHex(number, padding = 2) {
  var hex = Number(number).toString(16)
  while (hex.length < padding) {
    hex = `0${hex}`
  }

  return `0x${hex}`
}

module.exports = {
  convertNumberToHex
}