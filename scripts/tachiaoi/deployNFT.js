const hre = require('hardhat')
const { writeNetworkEnv, readNetworkEnv } = require('../network/env')
const { addressPage, isTomoChain } = require('../utils')

readNetworkEnv(hre.network)

function writeEnv(address) {
  writeNetworkEnv('NFT', address, hre.network)
  readNetworkEnv(hre.network)
}

async function argsInit() {
  const tokenAddress = process.env.TOKEN || hre.ethers.constants.AddressZero
  const taker = await hre.ethers.getSigner()
  const minFee = hre.ethers.utils.parseUnits('40').toString(10)

  return [
    'Test NFT', // name
    'TNFT', // symbol
    tokenAddress, // token
    taker.address, // taker address
    minFee, // min fee
  ]
}

// npx hardhat run ./scripts/deployNFT.js
async function main() {
  const nft = await (await hre.ethers.getContractFactory(
    isTomoChain(hre.network)
      ? 'contracts/0.4.26/MyNFT.sol:MyNFT'
      : 'contracts/0.8.11/MyNFT.sol:MyNFT'
  )).deploy(...(await argsInit()))


  writeEnv(nft.address)
  console.log(`Address: ${addressPage(hre.network, nft.address)}`)
}

main().catch((error) => {
  console.error(error)
  process.exit(1)
})