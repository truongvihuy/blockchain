const hre = require('hardhat')
const { writeNetworkEnv, readNetworkEnv } = require('../network/env')
const { addressPage, isTomoChain } = require('../utils')

readNetworkEnv(hre.network)

function writeEnv(address) {
  writeNetworkEnv('NFT_CONVERT', address, hre.network)
  readNetworkEnv(hre.network)
}

async function argsInit() {
  const tokenAddress = process.env.TOKEN || hre.ethers.constants.AddressZero
  const nftPackageAddress = process.env.NFT_PACKAGE || hre.ethers.constants.AddressZero
  const taker = await hre.ethers.getSigner()
  const minFee = hre.ethers.utils.parseUnits('0').toString()
  return [
    'NFT Convert', // name
    'NFTC', // symbol
    tokenAddress, // token
    taker.address, // taker address
    minFee, // min fee
    nftPackageAddress, // nft package
  ]
}

// npx hardhat run ./scripts/utils/deployNFTConvert.js
async function main() {
  const nftConvert = await (await hre.ethers.getContractFactory(
    isTomoChain(hre.network)
      ? 'contracts/0.4.26/NFTConvert.sol:NFTConvert'
      : 'contracts/0.8.11/NFTConvert.sol:NFTConvert'
  )).deploy(...(await argsInit()))

  writeEnv(nftConvert.address)
  console.log(`Address: ${addressPage(hre.network, nftConvert.address)}`)
}

main().catch((error) => {
  console.error(error)
  process.exit(1)
})