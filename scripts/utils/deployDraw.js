const hre = require('hardhat')
const { writeNetworkEnv, readNetworkEnv } = require('../../network/env')
const { addressPage, isTomoChain } = require('../../utils')

readNetworkEnv(hre.network)

function writeEnv(address) {
  writeNetworkEnv('DRAW', address, hre.network)
  readNetworkEnv(hre.network)
}

async function argsInit() {
  return [
    'ExchangeNFT', // name
    'ENFT', // symbol
  ]
}

// npx hardhat run ./scripts/utils/deployExchange.js
async function main() {
  const draw = await (await hre.ethers.getContractFactory('contracts/0.8.11/Draw.sol:Draw'))
    .deploy(...(await argsInit()))


  writeEnv(nft.address)
  console.log(`Address: ${addressPage(hre.network, draw.address)}`)
}

main().catch((error) => {
  console.error(error)
  process.exit(1)
})