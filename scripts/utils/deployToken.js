const hre = require('hardhat')
const { writeNetworkEnv, readNetworkEnv } = require('../network/env')
const { addressPage, isTomoChain } = require('../utils')

readNetworkEnv(hre.network)

function writeEnv(address) {
  writeNetworkEnv('TOKEN', address, hre.network)
  readNetworkEnv(hre.network)
}

async function argsInit() {
  return [
    'TvHuy 21',
    'TVH21',
  ]
}

// npx hardhat run ./scripts/utils/deployToken.js
async function main() {
  const token = await (await hre.ethers.getContractFactory(
    isTomoChain(hre.network)
      ? 'contracts/0.4.26/MyERC21.sol:MyERC21'
      : 'contracts/0.8.11/MyERC21.sol:MyERC21'
  )).deploy(...(await argsInit()))

  writeEnv(token.address)
  console.log(`Address: ${addressPage(hre.network, token.address)}`)
}

main().catch((error) => {
  console.error(error)
  process.exit(1)
})