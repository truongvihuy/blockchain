const hre = require('hardhat')
const { writeNetworkEnv, readNetworkEnv } = require('../network/env')
const { addressPage, isTomoChain } = require('../utils')

readNetworkEnv(hre.network)

function writeEnv(address) {
  writeNetworkEnv('NFT_PACKAGE', address, hre.network)
  readNetworkEnv(hre.network)
}

async function argsInit() {
  const tokenAddress = process.env.TOKEN || hre.ethers.constants.AddressZero
  const taker = await hre.ethers.getSigner()
  const minFee = hre.ethers.utils.parseUnits('0').toString()
  return [
    'NFT Package', // name
    'NFTP', // symbol
    taker.address, // ceo address
    taker.address, // creator address
    tokenAddress, // token
    taker.address, // taker address
    minFee, // min fee
  ]
}

// npx hardhat run ./scripts/utils/deployNFTPackage.js
async function main() {
  const nftPackage = await (await hre.ethers.getContractFactory(
    isTomoChain(hre.network)
      ? 'contracts/0.4.26/NFTPackage.sol:NFTPackage'
      : 'contracts/0.8.11/NFTPackage.sol:NFTPackage'
  )).deploy(...(await argsInit()))

  writeEnv(nftPackage.address)
  console.log(`Address: ${addressPage(hre.network, nftPackage.address)}`)
}

main().catch((error) => {
  console.error(error)
  process.exit(1)
})