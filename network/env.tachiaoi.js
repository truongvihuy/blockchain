const dotenv = require('dotenv')
const fs = require('fs')

function readNetworkEnv(network) {
  const networkName = network.name
  dotenv.config({ path: `./network/.env.${networkName}` })
}

function writeNetworkEnv(key, value, network) {
  try {
    let data = ''
    data += `YU=${(key !== 'TOKEN' ? process.env.TOKEN : value) ?? ''}\n`
    data += `YU2=${(key !== 'NFT' ? process.env.NFT : value) ?? ''}\n`
    data += `FEE=${(key !== 'NFT_PACKAGE' ? process.env.NFT_PACKAGE : value) ?? ''}\n`
    data += `NFT_CONVERT=${(key !== 'NFT_CONVERT' ? process.env.NFT_CONVERT : value) ?? ''}\n`
    data += `FIAT=${(key !== 'FIAT' ? process.env.FIAT : value) ?? ''}\n`
    data += `MARKET=${(key !== 'MARKET' ? process.env.MARKET : value) ?? ''}\n`
    data += `MARKET_SUB=${(key !== 'MARKET_SUB' ? process.env.MARKET_SUB : value) ?? ''}\n`
    fs.writeFileSync(`./network/.env.${network.name}`, data)
  } catch (e) {
    console.error(e)
  }
}

module.exports = {
  readNetworkEnv,
  writeNetworkEnv,
}