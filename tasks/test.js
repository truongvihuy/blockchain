const request = require('request')
const accountsPrivatesKey = require('../accounts/accounts.json')
// const accountsPrivatesKey = require('../accounts/accounts.testnet.json')
const { transactionPage } = require('../utils')
const abi = require('./abi.json')
const abi20 = require('../abi/ERC20.json')

const IPFS_UPLOAD_URL = "https://dev-ipfs.w3w.app/api/v0/add";

const uploadData = async ({ seriesId, buyer, tickets, paymentBy, totalAmount, timestamp }) => {
  return new Promise(resolve => {
    const r = request.post({ uri: IPFS_UPLOAD_URL }, function (
      err,
      httpResponse,
      body
    ) {
      const res = JSON.parse(body)
      console.log(`https://dev-ipfsgw.w3w.app/ipfs/${res.Hash}`);
      return resolve(res.Hash);
    });

    // eslint-disable-next-line no-undef
    const form = r.form();
    form.append("file", JSON.stringify({
      seriesId,
      buyer,
      tickets,
      paymentBy,
      totalAmount,
      timestamp,
    }));
  })
};

const API_URL = "https://dev-cig-api-v2.w3w.app/nft";

const callApi = async ({ oauth, data }) => {
  return new Promise(resolve => {

    const r = request.post({
      url: API_URL,
      headers: {
        'Authorization': oauth,
      },
      body: JSON.stringify(data)
    }, function (
      err,
      httpResponse,
      body
    ) {
      const res = (body)
      resolve(body)
    })
  })
}

const series = '1650960722560'
const loteAddress = '0xe29005506840cB4707FA3c761C403A37335E7b4E'
const usdtAddress = '0x013345B20fe7Cf68184005464FBF204D9aB88227'

// npx hardhat test
task('test', 'Test')
  .setAction(async function (_, hre) {
    const accounts = await hre.ethers.getSigners()
    const lote = new hre.ethers.Contract(
      loteAddress,
      abi,
      accounts[0],
    )
    const usdt = new hre.ethers.Contract(
      usdtAddress,
      abi20,
      accounts[0],
    )

    let num = 200000

    for (var i = 0; i < accounts.length; i++) {
      const numbers = (num + i).toString()
      const now = Date.now() * 1
      const hash = await uploadData({
        "seriesId": series,
        "buyer": accounts[i].address,
        "tickets": [
          {
            "numbers": numbers,
            "ticketId": now + 1,
            "status": "TICKET_SELLING"
          }
        ],
        "paymentBy": "USDT",
        "totalAmount": "1000000000000000000",
        "timestamp": now
      })



      const messageHash = 'Crypto Ishtar';
      const signature = await (new hre.ethers.Wallet(accountsPrivatesKey[i].privateKey)).signMessage(messageHash)
      const res = await callApi({
        oauth: `${signature}|${accounts[i].address}`,
        data: { "series": series, "buyer": accounts[i].address, "tickets": [{ "numbers": numbers, "ticketId": now + 1, "status": "TICKET_SELLING" }], "paymentBy": "USDT", "totalAmount": "1000000000000000000", "timestamp": now, "ipfsHash": hash }
      })
      console.log(res)

      // const txapprove = await (await usdt.connect(accounts[i]).approve(loteAddress, '999999999999999999999999999999999999')).wait()
      const tx = await (await lote.connect(accounts[i]).buy(series, hash, 3, 1)).wait()
      console.log(`TxID: ${transactionPage(hre.network, tx)}`)
      console.log('--------------------------')
    }
  })
