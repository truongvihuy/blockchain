const { types } = require('hardhat/config')
const { constants } = require('ethers/lib/ethers')
const { readNetworkEnv } = require('../network/env')
const { isTomoChain, transactionPage } = require('../utils')
const { address } = require('../utils/types')
const ABI = require('./abiPart2.json')
const accounts = require('../accounts/accounts.testnet.json')

const contractAddress = '0x421D34dD2FB285F929318A08861FeCb30BE7F8f8'


/**
 * Sign transaction
 * 
 * npx hardhat sign-nftpart --tokenids 1,2,3 --tokenamount 0
 */
task('sign-nftpart', 'Sign transaction')
  .addParam('tokenids', 'Ticket id')
  .addParam('tokenamount', 'Type ticket', 0, types.int)
  .setAction(async function (taskArgs, hre) {
    const timestamp = (new Date()) / 1000 | 0

    const Contract = new hre.web3.eth.Contract(
      ABI,
      contractAddress,
    )
    console.log(taskArgs)

    const hash = await Contract.methods.getMessageHash(taskArgs.tokenids.split(',').map(e => +e), taskArgs.tokenamount, timestamp).call()

    const sign = hre.web3.eth.accounts.sign(hash, accounts[0].privateKey)

    console.log('messageHash:', sign.message)
    console.log('timestamp:', timestamp)
    console.log('v:', +sign.v)
    console.log('r:', sign.r)
    console.log('s:', sign.s)
  })