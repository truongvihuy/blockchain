const { types } = require('hardhat/config')
const { constants } = require('ethers/lib/ethers')
const { readNetworkEnv } = require('../network/env')
const { isTomoChain, transactionPage } = require('../utils')
const { address } = require('../utils/types')
const ABI = require('./abiItem.json')
const accounts = require('../accounts/accounts.testnet.json')

const contractAddress = '0xce5c40007548D7Ab779dCEF3742703ffd375041F'


/**
 * Sign transaction
 * 
 * npx hardhat sign-nftitem --tokenids 1,2,3 --tokenamount 0
 */
task('sign-nftitem', 'Sign transaction')
  .addParam('tokenids', 'Ticket id')
  .addParam('tokenamount', 'Type ticket', 0, types.int)
  .setAction(async function (taskArgs, hre) {
    const timestamp = (new Date()) / 1000 | 0

    const Contract = new hre.web3.eth.Contract(
      ABI,
      contractAddress,
    )

    const hash = await Contract.methods.getMessageHash(taskArgs.tokenids.split(',').map(e => +e), taskArgs.tokenamount, timestamp).call()

    const sign = hre.web3.eth.accounts.sign(hash, accounts[0].privateKey)

    console.log('messageHash:', sign.message)
    console.log('timestamp:', timestamp)
    console.log('v:', +sign.v)
    console.log('r:', sign.r)
    console.log('s:', sign.s)
  })