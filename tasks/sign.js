const { types } = require('hardhat/config')
const { constants } = require('ethers/lib/ethers')
const { readNetworkEnv } = require('../network/env')
const { isTomoChain, transactionPage } = require('../utils')
const { address } = require('../utils/types')
const ABIDraw = require('./abiDraw.json')
const accounts = require('../accounts/accounts.testnet.json')

const contractDraw = '0xE4A7eAe601EE99B4D1D8A9A4ad3b68BaF21342C4'


/**
 * Sign transaction
 * 
 * npx hardhat sign --ticket 1 --typeticket 0
 */
task('sign', 'Sign transaction')
  .addParam('ticket', 'Ticket id', undefined, types.int)
  .addParam('typeticket', 'Type ticket', 0, types.int)
  .setAction(async function (taskArgs, hre) {
    const timestamp = (new Date()) / 1000 | 0

    const Draw = new hre.web3.eth.Contract(
      ABIDraw,
      contractDraw,
    )

    const hash = await Draw.methods.getMessageHash(taskArgs.ticket, taskArgs.typeticket, timestamp).call()

    const sign = hre.web3.eth.accounts.sign(hash, accounts[0].privateKey)

    console.log('messageHash:', sign.message)
    console.log('timestamp:', timestamp)
    console.log('v:', +sign.v)
    console.log('r:', sign.r)
    console.log('s:', sign.s)
  })